<?php

namespace MadBob\Sparqler\Literals;

use EasyRdf\Literal as ELiteral;

class Duration extends ELiteral
{
    public function __construct($value = null, $lang = null, $datatype = null)
    {
        if (is_null($value)) {
            $value = new \DateInterval('PT0F');
        }

        if ($value instanceof \DateInterval) {
            /*
                https://stackoverflow.com/a/61088115/3135371
            */
            $f = ['S0F', 'M0S', 'H0M', 'DT0H', 'M0D', 'P0Y', 'Y0M', 'P0M'];
            $r = ['S', 'M', 'H', 'DT', 'M', 'P', 'Y', 'P'];
            $value = rtrim(str_replace($f, $r, $value->format('P%yY%mM%dDT%hH%iM%sS%fF')), 'PT') ?: 'PT0F';
        }

        parent::__construct($value, null, $datatype);
    }

    public static function parse($value)
    {
        $value = DateInterval::createFromDateString($value);
        return new self($value);
    }

    public function getValue()
    {
        return new \DateInterval($this->value);
    }
}
