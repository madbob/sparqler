<?php

/*
    An InnerQuery is usually used in combination with an operator like FILTER,
    into which is directly exploded.
    Do not confuse with Subquery.
*/

namespace MadBob\Sparqler\Blocks;

use MadBob\Sparqler\Builder;

abstract class InnerQuery extends Triple
{
    protected $operator = '';
    protected $callable = null;

    protected function setCallable($callable)
    {
        $this->callable = $callable;
    }

    public function compile()
    {
        $internal_builder = new Builder($this->builder->getClient());
        $internal_builder->setMode('subquery');
        $internal_builder->setMasterSubject($this->builder->getMasterSubject());
        ($this->callable)($internal_builder);
        $internal_builder->compile();
        return sprintf('%s { %s }', $this->operator, $internal_builder->toString());
    }
}
