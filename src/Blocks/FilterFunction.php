<?php

namespace MadBob\Sparqler\Blocks;

use MadBob\Sparqler\Terms\Term;
use MadBob\Sparqler\Terms\Raw;

class FilterFunction extends Triple
{
    use Filter;

    protected $operator;

    public function __construct($builder, $predicate, $operator, $object)
    {
        parent::__construct($builder, $predicate, $operator, $object);
        $this->builder = $builder;
        $this->condition = '';

        if (is_a($operator, Raw::class)) {
            $this->operator = $operator->compile();
        }
        else {
            $this->operator = $operator;
        }
    }

    public function compile()
    {
        switch ($this->operator) {
            case 'lang':
                $ret = sprintf('FILTER ( langMatches(lang(%s), %s) )', $this->subject->compile(), $this->object->compile());
                break;

            case 'regex':
                $ret = sprintf('FILTER ( regex(%s, %s) )', $this->subject->compile(), $this->object->compile());
                break;

            case 'in':
                $objects = [];

                foreach ($this->object as $obj) {
                    $objects[] = $obj->compile();
                }

                $ret = sprintf('FILTER ( %s IN (%s) )', $this->subject->compile(), join(', ', $objects));
                break;

            default:
                $parent = $this;

                $filter = new FilterQuery($parent->builder, $parent->condition, function ($query) use ($parent) {
                    $query->where($parent->subject, $parent->predicate, $parent->object);
                });

                $ret = $filter->compile();
                break;
        }

        return $ret;
    }
}
