<?php

namespace MadBob\Sparqler\Blocks;

class WhereOptional extends Where
{
    public function compile()
    {
        $query = parent::compile();
        if (empty($query) === false) {
            return sprintf('OPTIONAL { %s }', $query);
        }
        else {
            return '';
        }
    }
}
