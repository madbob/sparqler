<?php

namespace MadBob\Sparqler\Blocks;

class FilterQuery extends InnerQuery
{
    use Filter {
        setCondition as realSetCondition;
    }

    public function __construct($builder, $callable)
    {
        $this->builder = $builder;
        $this->operator = 'FILTER';
        $this->setCallable($callable);
    }

    public function setCondition($condition)
    {
        $this->realSetCondition($condition);
        $this->operator = sprintf('FILTER %s', $this->condition);
    }
}
