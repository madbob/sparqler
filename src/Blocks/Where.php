<?php

/*
    A Where is basically a Triple, but with an implicit subject (the main one
    from the Builder) and an operator (which alters the behaviour of the final
    output).
*/

namespace MadBob\Sparqler\Blocks;

use MadBob\Sparqler\Terms\Term;
use MadBob\Sparqler\Terms\Variable;

class Where extends Triple
{
    private $operator;

    public function __construct($builder, $predicate, $operator, $object)
    {
        parent::__construct($builder, null, $predicate, $object);
        $this->builder = $builder;
        $this->operator = $operator;
    }

    private function prepareFilter()
    {
        $ret = '';

        list($temp, $exists) = $this->builder->mapPredicateToVariable($this->predicate);
        if ($exists === false) {
            $ret = sprintf('%s %s %s . ', $this->getSubject()->compile(), $this->predicate->compile(), $temp->compile());
        }

        return [$ret, $temp];
    }

    public function compile()
    {
        switch ($this->operator) {
            case '=':
                $ret = parent::compile();
                break;

            case '<':
            case '>':
            case '<=':
            case '>=':
            case '!=':
                list($ret, $temp) = $this->prepareFilter();
                $ret = $ret .= sprintf('FILTER ( %s %s %s )', $temp->compile(), $this->operator, $this->object->compile());
                break;

            case 'regex':
                list($ret, $temp) = $this->prepareFilter();
                $ret = $ret .= sprintf('FILTER regex ( %s, %s )', $temp->compile(), $this->object->compile());
                break;

            case 'in':
                list($ret, $temp) = $this->prepareFilter();

                $objects = [];
                foreach ($this->object as $obj) {
                    $objects[] = $obj->compile();
                }

                $ret = $ret .= sprintf('VALUES %s { %s }', $temp->compile(), join(' ', $objects));
                break;

            case 'raw':
                $ret = $this->predicate;
                break;

            default:
                /*
                    This is to handle the case in which where() is used as a
                    pure triple, with explicit subject, predicate and object
                */
                if (is_a($this->operator, Term::class)) {
                    $this->setSubject($this->predicate);
                    $this->setPredicate($this->operator);
                    $this->operator = null;
                }

                $ret = parent::compile();
                break;
        }

        return $ret;
    }
}
