<?php

namespace MadBob\Sparqler\Blocks;

trait Filter
{
    protected $condition;

    public function setCondition($condition)
    {
        $this->condition = $condition;
    }
}
