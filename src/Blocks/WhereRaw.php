<?php

namespace MadBob\Sparqler\Blocks;

class WhereRaw extends Where
{
    private $condition;

    public function __construct($builder, $condition)
    {
        $this->condition = $condition;
    }

    public function compile()
    {
        return $this->condition;
    }
}
