<?php

namespace MadBob\Sparqler\Blocks;

class Minus extends InnerQuery
{
    public function __construct($builder, $callable)
    {
        $this->builder = $builder;
        $this->operator = 'MINUS';
        $this->setCallable($callable);
    }
}
