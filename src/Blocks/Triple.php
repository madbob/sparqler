<?php

namespace MadBob\Sparqler\Blocks;

use MadBob\Sparqler\Terms\Term;
use MadBob\Sparqler\Terms\Variable;
use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Terms\Subquery;

class Triple
{
    protected $builder;
    protected $subject;
    protected $predicate;
    protected $object;

    public function __construct($builder, $subject, $predicate, $object)
    {
        $this->builder = $builder;

        if (is_null($subject)) {
            $subject = $this->builder->getMasterSubject();
        }
        else {
            $subject = $this->builder->enclose($subject, Variable::class);
        }

        $this->set('subject', $subject);
        $this->set('predicate', $this->builder->enclose($predicate, Iri::class));
        $this->set('object', $this->builder->enclose($object));
    }

    public function setSubject($subject)
    {
        $this->set('subject', $subject);
    }

    public function setPredicate($predicate)
    {
        $this->set('predicate', $predicate);
    }

    public function setObject($object)
    {
        $this->set('object', $object);
    }

    public function set($position, $value)
    {
        $this->$position = $value;

        if (is_a($this->$position, Term::class)) {
            $this->$position->setTriple($this);
        }
    }

    public function getSubject()
    {
        return $this->get('subject');
    }

    public function getPredicate()
    {
        return $this->get('predicate');
    }

    public function getObject()
    {
        return $this->get('object');
    }

    public function get($position)
    {
        return $this->$position;
    }

    public function equals($triple)
    {
        if (is_null($triple)) {
            return false;
        }

        $subject = $this->getSubject();
        $predicate = $this->getPredicate();
        $object = $this->getObject();

        $triple_subject = $triple->getSubject();
        $triple_predicate = $triple->getPredicate();
        $triple_object = $triple->getObject();

        return (
            $subject && $triple_subject && $subject->sameAs($triple_subject)
            && $predicate && $triple_predicate && $predicate->sameAs($triple_predicate)
            && $object && $triple_object && $object->sameAs($triple_object)
        );
    }

    public function each($callback)
    {
        $positions = ['subject', 'predicate', 'object'];
        foreach ($positions as $position) {
            $a = $this->get($position);
            if ($callback($position, $a) === false) {
                break;
            }
        }
    }

    public function compile()
    {
        return sprintf('%s %s %s', $this->subject->compile(), $this->predicate->compile(), $this->object->compile());
    }
}
