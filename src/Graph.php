<?php

namespace MadBob\Sparqler;

use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Terms\Variable;
use Iterator;
use EasyRdf\Graph as EGraph;

class Graph extends EGraph implements Iterator
{
    private $parsing = false;

    private $client;

    private $adds = [];
    private $deletes = [];

    private $iterable_resources = null;
    private $iterable_offset = 0;

    public static function extend($client, $real_graph)
    {
        if ($real_graph) {
            /*
                Sometime EasyRDF parser fails to serialize and parse the same data
                back in certain formats, here we have at least two options as a
                fallback
            */
            try {
                $contents = $real_graph->serialise('json');
                $ret = new self($real_graph->getUri(), $contents, null);
            }
            catch (\Exception $e) {
                $contents = $real_graph->serialise('turtle');
                $ret = new self($real_graph->getUri(), $contents, null);
            }
        }
        else {
            $ret = new self();
        }

        $ret->client = $client;

        return $ret;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function parse($data, $format = null, $uri = null)
    {
        $this->internalOps(true);
        parent::parse($data, $format, $uri);
        $this->internalOps(false);
    }

    public function internalOps($internal)
    {
        $this->parsing = $internal;
    }

    public function add($resource, $property, $value)
    {
        if ($this->parsing === false) {
            /*
                $value here is usually an associative array, then internally
                translated in EasyRdf\Graph::arrayToObject().
                The array here is left untouched for maximum flexibility, and
                eventually converted back in Builder::enclose() to a meaninful
                object
            */
            $this->adds[] = [new Iri($resource), new Iri($property), $value];
        }

        return parent::add($resource, $property, $value);
    }

    public function deleteSingleProperty($resource, $property, $value = null)
    {
        if ($this->parsing === false) {
            if ($value) {
                $this->deletes[] = [new Iri($resource), new Iri($property), $value];
            }
            else {
                $this->deletes[] = [new Iri($resource), new Iri($property), new Variable()];
            }
        }

        return parent::deleteSingleProperty($resource, $property, $value);
    }

    public function commit()
    {
        /*
            When a property's value is modified in the graph, the old value is
            removed passing through deleteSingleProperty(). But the value may
            not be already defined!
            Calling directly
            $this->client->doDelete($this->deletes)->queue();
            the whole set of modified properties is marked once for deletion,
            but if even a single one is not found on the data base the whole
            DELETE query fails and also the existing old values we want to
            remove stay in place.
            So, here we execute a distinct DELETE query for each property: those
            targeting missing values will quietly fails, but others will be
            executed.
        */
        foreach ($this->deletes as $d) {
            $this->client->doDelete([$d])->queue();
        }

        if (!empty($this->adds)) {
            $this->client->doInsert($this->adds)->queue();
        }

        $this->client->runQueue();
    }

    /*
        The native resources() function returns all Resources into the Graph,
        including those enumerated as properties. This function filters only the
        primary Resources, those directly targeted by the generating query
    */
    public function masterResources()
    {
        $ret = [];

        foreach ($this->toRdfPhp() as $subject => $properties) {
            $ret[] = $this->resource($subject);
        }

        return $ret;
    }

    private function initIterable()
    {
        if (is_null($this->iterable_resources)) {
            $this->iterable_resources = $this->masterResources();
            $this->iterable_offset = 0;
        }
    }

    public function current()
    {
        $this->initIterable();
        return $this->iterable_resources[$this->iterable_offset];
    }

    public function key()
    {
        return $this->iterable_offset;
    }

    public function next()
    {
        $this->iterable_offset++;
    }

    public function rewind()
    {
        $this->iterable_offset = 0;
    }

    public function valid()
    {
        $this->initIterable();
        return isset($this->iterable_resources[$this->iterable_offset]);
    }
}
