<?php

namespace MadBob\Sparqler;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\NullLogger;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Terms\OwnSubject;
use EasyRdf\Graph as EGraph;

class Client extends \EasyRdf\Sparql\Client implements LoggerAwareInterface
{
    private $httpclient = null;
    private $config = null;
    private $ontology = null;
    private $graph = null;
    private $logger = false;
    private $cache = false;
    private $queue = [];
    private static $globalInstance = null;

    public function __construct($config)
    {
        $this->httpclient = new \MadBob\EasyRDFonGuzzle\HttpClient();

        if (isset($config['namespaces'])) {
            foreach (\EasyRdf\RdfNamespace::namespaces() as $prefix => $url) {
                \EasyRdf\RdfNamespace::delete($prefix);
            }

            foreach ($config['namespaces'] as $prefix => $uri) {
                \EasyRdf\RdfNamespace::set($prefix, $uri);
            }
        }
        else {
            $config['namespaces'] = \EasyRdf\RdfNamespace::namespaces();
        }

        if (isset($config['graph'])) {
            $this->graph = $config['graph'];
        }

        $handler = new \GuzzleHttp\Handler\CurlHandler();
        $stack = \GuzzleHttp\HandlerStack::create($handler);
        $this->httpclient->setOptions('handler', $stack);

        if (isset($config['auth'])) {
            $this->httpclient->setOptions('auth', [$config['auth']['username'], $config['auth']['password'], $config['auth']['type']]);
        }

        \EasyRdf\Http::setDefaultHttpClient($this->httpclient);

        \EasyRdf\TypeMapper::setDefaultResourceClass(Resource::class);
        \EasyRdf\TypeMapper::set('rdf:Alt', Container::class);
        \EasyRdf\TypeMapper::set('rdf:Bag', Container::class);
        \EasyRdf\TypeMapper::set('rdf:List', Collection::class);
        \EasyRdf\TypeMapper::set('rdf:Seq', Container::class);

        \EasyRdf\Literal::setDatatypeMapping('xsd:nonNegativeInteger', \EasyRdf\Literal\Integer::class);
        \EasyRdf\Literal::setDatatypeMapping('xsd:positiveInteger', \EasyRdf\Literal\Integer::class);
        \EasyRdf\Literal::setDatatypeMapping('xsd:duration', \MadBob\Sparqler\Literals\Duration::class);

        $this->logger = new \Psr\Log\NullLogger();

        $this->config = $config;

        parent::__construct($config['host']);
    }

    public function setAsGlobal()
    {
        self::$globalInstance = $this;
    }

    public static function globalInstance()
    {
        return self::$globalInstance;
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    public function getLogger()
    {
        return $this->logger;
    }

    public function setCache(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    public function getCache()
    {
        return $this->cache;
    }

    public function getGraph()
    {
        return $this->graph;
    }

    public function getOntology()
    {
        if (is_null($this->ontology)) {
            $this->ontology = new Ontology($this, $this->config);
        }

        return $this->ontology;
    }

    private function initBuilder($mode, $modevars)
    {
        $ret = new Builder($this);
        $ret->setGraph($this->graph);
        $ret->setMode($mode, $modevars);

        if ($this->config['omit_prefix'] ?? false) {
            $ret->prefix('');
        }

        return $ret;
    }

    public function doSelect($vars = [])
    {
        if (empty($vars)) {
            $vars = [new OwnSubject()];
        }

        return $this->initBuilder('select', $vars);
    }

    public function doSelectDistinct($vars = [])
    {
        if (empty($vars)) {
            $vars = [new OwnSubject()];
        }

        return $this->initBuilder('select_distinct', $vars);
    }

    public function doConstruct($vars = [])
    {
        return $this->initBuilder('construct', $vars);
    }

    public function doDelete($vars = [])
    {
        return $this->initBuilder('delete', $vars);
    }

    public function doInsert($vars)
    {
        return $this->initBuilder('insert', $vars);
    }

    public function doRaw($sparql)
    {
        $result = $this->query($sparql);

        if (is_a($result, EGraph::class)) {
            $result = Graph::extend($this, $result);
        }

        return $result;
    }

    public function find($uri)
    {
        if (is_string($uri)) {
            $uri = new Iri($uri);
        }

        $query = $this->doConstruct();
        $query->setMasterSubject($uri);

        $graph = $query->get();
        if (count($graph->resources()) == 0) {
            return null;
        }

        return $graph->resource($uri->compile());
    }

    public function queue($builder)
    {
        $this->queue[] = $builder;
    }

    public function runQueue()
    {
        $queries = [];

        foreach ($this->queue as $index => $query) {
            /*
                Here is assumed that all queued queries have the same PREFIX, so
                it is left only in the first query and invalidated in all others
            */
            if ($index != 0) {
                $query->prefix('');
            }

            $queries[] = $query->toString();
        }

        $this->update(join("\n;\n", $queries));
        $this->queue = [];
    }

    /*
        This function just wraps the native request() to collect logs
    */
    protected function request($type, $query)
    {
        $this->getLogger()->info($query);
        return parent::request($type, $query);
    }
}
