<?php

namespace MadBob\Sparqler;

use EasyRdf\TypeMapper;
use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Terms\OwnSubject;

abstract class Model extends Resource
{
    protected $class = '';
    protected $iriPrefix = '';

    protected function newIri()
    {
        return $this->iriPrefix . uuid_create(UUID_TYPE_RANDOM);
    }

    public function __construct($uri = null, $graph = null)
    {
        $test = TypeMapper::get($this->class);
        if (is_null($test)) {
            TypeMapper::set($this->class, static::class);
        }

        if (is_null($uri)) {
            $uri = $this->newIri();
        }

        if (is_null($graph)) {
            $client = self::getGlobalClient();
            $graph = Graph::extend($client, null);
        }

        parent::__construct($uri, $graph);
    }

    private static function getGlobalClient()
    {
        $ret = Client::globalInstance();

        if (is_null($ret)) {
            throw new \LogicException("No Client has been defined as global using setAsGlobal()");
        }

        return $ret;
    }

    public static function query($vars = [])
    {
        $myself = new static();

        /*
            If specific properties are required, I enforce here to fetch also
            rdf:type to leverage TypeMapper mapping when the result set is read
            back and ensure that the final Graph is properly populated with the
            right type of Resource
        */
        if (empty($vars) === false) {
            $vars[] = ['rdf:type'];
        }

        return self::getGlobalClient()->doConstruct($vars)->where('rdf:type', new Iri($myself->class));
    }

    public static function find($iri)
    {
        $myself = new static();
        return self::getGlobalClient()->find($iri);
    }

    public function __get($property)
    {
        switch ($property) {
            case 'id':
                return $this->getUri();
        }

        return null;
    }

    public function save()
    {
        $this->graph->commit();
    }
}
