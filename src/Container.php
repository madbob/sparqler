<?php

/*
    Original code:

    EasyRdf
    Copyright (c) Nicholas J Humfrey
    https://www.opensource.org/licenses/bsd-license.php
*/

namespace MadBob\Sparqler;

class Container extends Resource implements \ArrayAccess, \Countable, \SeekableIterator
{
    private $position;

    public function __construct($uri, $graph)
    {
        $this->position = 1;
        parent::__construct($uri, $graph);
    }

    public function seek($position)
    {
        if (is_int($position) && $position > 0) {
            if ($this->hasProperty('rdf:_'.$position)) {
                $this->position = $position;
            }
            else {
                throw new \OutOfBoundsException("Unable to seek to position $position in the container");
            }
        }
        else {
            throw new \InvalidArgumentException("Container position must be a positive integer");
        }
    }

    public function rewind()
    {
        $this->position = 1;
    }

    public function current()
    {
        return $this->get('rdf:_'.$this->position);
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        $this->position++;
    }

    public function valid()
    {
        return $this->hasProperty('rdf:_'.$this->position);
    }

    public function count()
    {
        $pos = 1;

        while ($this->hasProperty('rdf:_'.$pos)) {
            $pos++;
        }

        return $pos - 1;
    }

    public function append($value)
    {
        $pos = 1;

        while ($this->hasProperty('rdf:_'.$pos)) {
            $pos++;
        }

        return $this->add('rdf:_'.$pos, $value);
    }

    public function offsetExists($offset)
    {
        if (is_int($offset) && $offset > 0) {
            return $this->hasProperty('rdf:_'.$offset);
        }
        else {
            throw new \InvalidArgumentException("Container position must be a positive integer");
        }
    }

    public function offsetGet($offset)
    {
        if (is_int($offset) && $offset > 0) {
            return $this->get('rdf:_'.$offset);
        }
        else {
            throw new \InvalidArgumentException("Container position must be a positive integer");
        }
    }

    public function offsetSet($offset, $value)
    {
        if (is_int($offset) && $offset > 0) {
            return $this->set('rdf:_'.$offset, $value);
        }
        elseif (is_null($offset)) {
            return $this->append($value);
        }
        else {
            throw new \InvalidArgumentException("Container position must be a positive integer");
        }
    }

    public function offsetUnset($offset)
    {
        if (is_int($offset) && $offset > 0) {
            return $this->delete('rdf:_'.$offset);
        }
        else {
            throw new \InvalidArgumentException("Container position must be a positive integer");
        }
    }
}
