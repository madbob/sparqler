<?php

namespace MadBob\Sparqler\Terms;

class Aggregate implements Term
{
    use CoreTerm;

    private $function;
    private $value;
    private $as;

    public function __construct($function, $value, $as = null)
    {
        $this->function = mb_strtoupper($function);
        $this->value = $value;
        $this->as = $as;
    }

    protected function onBuilderSet()
    {
        $builder = $this->getBuilder();
        $this->value = $builder->enclose($this->value, Iri::class);

        if ($this->as) {
            $this->as = $builder->enclose($this->as, Variable::class);
        }
    }

    public function getTarget()
    {
        return $this->value;
    }

    public function wireBack($variable)
    {
        $this->value = $variable;
    }

    public function compile()
    {
        if ($this->as) {
            $as = $this->as->compile();
        }
        else {
            $as = sprintf('?%s', $this->function);
        }

        return sprintf('(%s(%s) as %s)', $this->function, $this->value->compile(), $as);
    }
}
