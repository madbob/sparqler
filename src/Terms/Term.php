<?php

namespace MadBob\Sparqler\Terms;

interface Term
{
    public function compile();
}
