<?php

namespace MadBob\Sparqler\Terms;

use EasyRdf\RdfNamespace;
use EasyRdf\Resource;

class Iri implements Term
{
    use CoreTerm;

    private $value;
    private $shorten;

    public function __construct($value)
    {
        if (is_object($value) && ($value instanceof Resource)) {
            $value = $value->getUri();
        }

        $shorten = RdfNamespace::shorten($value);
        if (is_null($shorten)) {
            $already_short = false;

            $namespaces = RdfNamespace::namespaces();
            foreach ($namespaces as $prefix => $uri) {
                $prefix_length = strlen($prefix);
                if (substr($value, 0, $prefix_length) == $prefix && $value[$prefix_length] == ':') {
                    $this->shorten = true;
                    $this->value = $value;
                    $already_short = true;
                    break;
                }
            }

            if ($already_short === false) {
                $this->shorten = false;
                $this->value = $value;
            }
        }
        else {
            $this->shorten = true;
            $this->value = $shorten;
        }
    }

    public function compile()
    {
        if ($this->shorten) {
            return $this->value;
        }
        else {
            return sprintf('<%s>', $this->value);
        }
    }

    public function expanded()
    {
        if ($this->shorten) {
            return RdfNamespace::expand($this->value);
        }
        else {
            return $this->value;
        }
    }
}
