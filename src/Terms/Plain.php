<?php

namespace MadBob\Sparqler\Terms;

class Plain implements Term
{
    use CoreTerm;

    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function compile()
    {
        return sprintf('"%s"', str_replace('"', '\"', $this->value));
    }
}
