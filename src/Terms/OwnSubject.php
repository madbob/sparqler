<?php

namespace MadBob\Sparqler\Terms;

class OwnSubject implements Term
{
    use CoreTerm;

    private $explicit = null;

    public function __construct($explicit = null)
    {
        $this->explicit = $explicit;
    }

    protected function onBuilderSet()
    {
        /*
            This has to be called as soon as possible into the Builder, to
            propagate the name of the subject variable to other filters
        */
        if ($this->explicit) {
            $this->getBuilder()->setMasterSubject(new Variable($this->explicit));
        }
    }

    public function compile()
    {
        return $this->getBuilder()->getMasterSubject()->compile();
    }

    public function raw()
    {
        return $this->getBuilder()->getMasterSubject()->raw();
    }
}
