<?php

namespace MadBob\Sparqler\Terms;

class Raw implements Term
{
    use CoreTerm;

    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function compile()
    {
        return $this->value;
    }
}
