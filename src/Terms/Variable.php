<?php

namespace MadBob\Sparqler\Terms;

use MadBob\Sparqler\Utils;

class Variable implements Term
{
    use CoreTerm;

    private $value;
    private $translate = null;
    private $explicit = false;

    public function __construct($value = null)
    {
        $this->value = $value;
        if ($value) {
            $this->explicit = true;
        }
    }

    public function compile()
    {
        if ($this->value == 'subject') {
            return $this->builder->getMasterSubject()->compile();
        }

        if (is_null($this->value)) {
            $this->value = Utils::randomString();
        }

        return sprintf('?%s', $this->value);
    }

    public function as($translate)
    {
        $this->translate = $translate;
    }

    public function translate()
    {
        return $this->translate;
    }

    public function raw()
    {
        $this->compile();
        return $this->value;
    }

    public function isExplicit()
    {
        return $this->explicit;
    }
}
