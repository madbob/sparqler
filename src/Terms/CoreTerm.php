<?php

namespace MadBob\Sparqler\Terms;

trait CoreTerm
{
    protected $builder;
    protected $triple;

    public function setBuilder($builder)
    {
        if (is_null($this->builder) || $this->builder->sameAs($builder) === false) {
            $this->builder = $builder;
            $this->onBuilderSet();
        }
    }

    public function getBuilder()
    {
        return $this->builder;
    }

    /*
        Sometime it is required that a Term is aware of the Triple in which is
        included. E.g. Subqueries need to alter the Triple itself to append a
        new Variable and use it as their own OwnSubject
    */
    public function setTriple($triple)
    {
        if ($triple->equals($this->triple) === false) {
            $this->triple = $triple;
            $this->onTripleSet();
        }
    }

    public function getTriple()
    {
        return $this->triple;
    }

    public function sameAs($term)
    {
        if (is_a($term, Term::class) === false) {
            return false;
        }
        else {
            return (get_class($this) == get_class($term) && $this->compile() == $term->compile());
        }
    }

    /*
        Overwrite this to make some action as soon as the Builder is attached
    */
    protected function onBuilderSet()
    {
        // dummy
    }

    /*
        Overwrite this to make some action as soon as the Triple is attached.
        Triple is set after the Builder
    */
    protected function onTripleSet()
    {
        // dummy
    }
}
