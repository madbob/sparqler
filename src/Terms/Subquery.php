<?php

/*
    When asked for compile, the Subquery generates a new Variable which is
    placed into the parent query. This Variable is the master subject of the
    Subquery itself, which is then actually compiled and appended to the parent
    query in the final phase of building.
    Do not confuse with InnerQuery.
*/

namespace MadBob\Sparqler\Terms;

use MadBob\Sparqler\Builder;
use MadBob\Sparqler\Building\BuildingRollback;

class Subquery implements Term
{
    use CoreTerm;

    private $value;
    private $placeholder_variable;
    private $executed = false;

    public function __construct($value)
    {
        $this->value = $value;
        $this->placeholder_variable = new Variable();
    }

    protected function onBuilderSet()
    {
        if ($this->executed === false) {
            $this->getBuilder()->queueFinal(function ($builder) {
                $internal_builder = new Builder($builder->getClient());
                $internal_builder->setMode('subquery');

                $internal_builder->setMasterSubject($this->placeholder_variable);
                ($this->value)($internal_builder);
                $internal_builder->compile();
                $builder->merge($internal_builder);

                return Builder::BUILDING_STEP_FILTERS;
            });

            $this->executed = false;
        }
    }

    protected function onTripleSet()
    {
        $predicate = $this->triple->getPredicate();
        if ($predicate) {
            list($var, $exists) = $this->builder->mapPredicateToVariable($predicate);
            $this->placeholder_variable = $var;
        }
    }

    public function compile()
    {
        return $this->placeholder_variable->compile();
    }
}
