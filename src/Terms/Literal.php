<?php

/*
    This is just a wrapper around EasyRdf\Literal to make it compatible with the
    Term interface
*/

namespace MadBob\Sparqler\Terms;

use EasyRdf\Literal as ELiteral;

class Literal extends ELiteral implements Term
{
    use CoreTerm;

    private $innerValue;

    public function __construct($value)
    {
        if (is_a($value, ELiteral::class) === false) {
            $value = Eliteral::create($value);
        }

        $this->innerValue = $value;
    }

    public function compile()
    {
        return $this->innerValue->dumpValue('text');
    }

    public function __call($function, $args)
    {
        return $this->innerValue->$function(...$args);
    }
}
