<?php

namespace MadBob\Sparqler\Utils;

class TermsArray implements \ArrayAccess, \Iterator, \Countable
{
    private $builder;
    private $container;
    private $position;
    private $preferred_class;

    public function __construct($builder, $preferred_class)
    {
        $this->builder = $builder;
        $this->container = [];
        $this->position = 0;
        $this->preferred_class = $preferred_class;
    }

    public function has($term)
    {
        $term = $this->builder->enclose($term, $this->preferred_class);

        foreach ($this->container as $c) {
            if ($c->sameAs($term)) {
                return true;
            }
        }

        return false;
    }

    public function add($term)
    {
        if ($this->has($term) === false) {
            $term = $this->builder->enclose($term, $this->preferred_class);
            $this->container[] = $term;
        }
    }

    public function count()
    {
        return count($this->container);
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->container[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->container[$this->position]);
    }

    public function offsetSet($offset, $value)
    {
        $this->container[$offset] = $value;
    }

    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }
}
