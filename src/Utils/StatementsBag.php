<?php

namespace MadBob\Sparqler\Utils;

use MadBob\Sparqler\Terms\Variable;
use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Blocks\Triple;
use MadBob\Sparqler\Blocks\Where;
use MadBob\Sparqler\Blocks\WhereOptional;
use MadBob\Sparqler\Blocks\WhereRaw;

class StatementsBag
{
    private $builder;
    private $statements;

    public function __construct($builder)
    {
        $this->builder = $builder;
        $this->statements = [];
    }

    public function isEmpty()
    {
        return empty($this->statements);
    }

    /*
        $statement must be an array. Depending on the count of items is the
        array, to each item is assigned a meaning and missing elements are
        guessed to build a Triple (made, of course, of three items)
    */
    public function add($statement)
    {
        if (is_a($statement, Triple::class)) {
            $this->statements[] = $statement;
        }
        else {
            switch (count($statement)) {
                case 1:
                    $subject = $this->builder->getMasterSubject();
                    $predicate = $this->builder->enclose($statement[0], Iri::class);
                    $object = new Variable();
                    break;

                case 2:
                    $subject = $this->builder->getMasterSubject();
                    $predicate = $this->builder->enclose($statement[0], Iri::class);
                    $object = $this->builder->enclose($statement[1]);
                    break;

                case 3:
                    $subject = $this->builder->enclose($statement[0], Variable::class);
                    $predicate = $this->builder->enclose($statement[1], Iri::class);
                    $object = $this->builder->enclose($statement[2]);
                    break;

                default:
                    throw new \InvalidArgumentException("Provided a triple with more than 3 items");
                    break;
            }

            $this->statements[] = new Triple($this->builder, $subject, $predicate, $object);
        }
    }

    public function addTriple($triple)
    {
        foreach ($this->statements as $item) {
            if ($item->equals($triple)) {
                return;
            }
        }

        $this->statements[] = $triple;
    }

    public function getTriples()
    {
        return $this->statements;
    }

    public function merge($statements)
    {
        $this->statements = array_merge($this->statements, $statements->statements);
    }

    public function compile()
    {
        /*
            It is better to move optional filters at the end of the query, it
            permits easier optimization by the SPARQL endpoint
        */
        usort($this->statements, function ($first, $second) {
            $sorting = [Where::class, WhereRaw::class, WhereOptional::class];
            $first_order = array_search(get_class($first), $sorting);
            $second_order = array_search(get_class($second), $sorting);
            return $first_order <=> $second_order;
        });

        $as_strings = [];

        foreach ($this->statements as $statement) {
            $as_strings[] = $statement->compile();
        }

        return join(' . ', $as_strings);
    }

    public function filterWithVariables()
    {
        $ret = new self($this->builder);

        foreach ($this->statements as $item) {
            if (is_a($item->getSubject(), Variable::class) || is_a($item->getPredicate(), Variable::class) || is_a($item->getObject(), Variable::class)) {
                $ret->addTriple($item);
            }
        }

        return $ret;
    }

    public function findVariable($variable)
    {
        $positions = ['subject', 'predicate', 'object'];

        foreach ($this->statements as $item) {
            foreach ($positions as $position) {
                $p = $item->get($position);
                if ($p && $p->sameAs($variable)) {
                    return $p;
                }
            }
        }

        return null;
    }

    private function matching($search, $return, $return_class, $predicate)
    {
        foreach ($this->statements as $item) {
            $p = $item->get($search);
            if ($p && $p->sameAs($predicate)) {
                $ret = $item->get($return);
                if (is_a($ret, $return_class)) {
                    return $ret;
                }
            }
        }

        return null;
    }

    public function matchingSubject($predicate)
    {
        return $this->matching('subject', 'predicate', Iri::class, $predicate);
    }

    public function matchingPredicate($predicate)
    {
        return $this->matching('predicate', 'object', Variable::class, $predicate);
    }

    public function matchingObject($predicate)
    {
        return $this->matching('object', 'predicate', Iri::class, $predicate);
    }
}
