<?php

/*
    Absolutely incomplete, unaffordable and partial!!!
    The Reasoner "explodes" a StatementsBag to include into the final query
    classes and properties not explicitely required but connected through the
    semantic relations extrapolated by the Ontology
*/

namespace MadBob\Sparqler\Utils;

use MadBob\Sparqler\Terms\Variable;
use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Blocks\Triple;
use MadBob\Sparqler\Blocks\FilterFunction;

class Reasoner
{
    private $builder = null;
    private $ontology = null;

    public function __construct($builder)
    {
        $this->builder = $builder;
        $this->ontology = $builder->getClient()->getOntology();
    }

    private function ontologyParentsChain($predicate, &$accumulator)
    {
        foreach ($predicate['parents'] as $parent) {
            if (in_array($parent, $accumulator) === false) {
                $accumulator[] = $parent;
                $parent_predicate = $this->ontology->getByIri($parent);
                $this->ontologyParentsChain($parent_predicate, $accumulator);
            }
        }
    }

    private function explodeIri($statements, $statement, $predicate, $position)
    {
        if (is_a($predicate, Iri::class)) {
            $ontology_predicate = $this->ontology->getByIri($predicate);

            if ($ontology_predicate) {
                $equivalents = $ontology_predicate['equivalents'];
                $this->ontologyParentsChain($ontology_predicate, $equivalents);

                if (!empty($equivalents)) {
                    $variable = new Variable();
                    $statement->set($position, $variable);

                    $equivalents = array_map(function ($pred) {
                        return new Iri($pred);
                    }, $equivalents);

                    $equivalents[] = $predicate;

                    $filter_equivalents = new FilterFunction($this->builder, $variable, 'in', $equivalents);
                    $statements->addTriple($filter_equivalents);
                }
            }
        }
    }

    public function infere($statements)
    {
        foreach ($statements->getTriples() as $statement) {
            $predicate = $statement->getPredicate();
            $this->explodeIri($statements, $statement, $predicate, 'predicate');
            $predicate = $statement->getObject();
            $this->explodeIri($statements, $statement, $predicate, 'object');
        }
    }
}
