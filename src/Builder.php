<?php

namespace MadBob\Sparqler;

use MadBob\Sparqler\Terms\Term;
use MadBob\Sparqler\Terms\Variable;
use MadBob\Sparqler\Terms\OwnSubject;
use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Terms\Optional;
use MadBob\Sparqler\Terms\Literal;
use MadBob\Sparqler\Terms\Plain;
use MadBob\Sparqler\Terms\Raw;
use MadBob\Sparqler\Terms\Aggregate;
use MadBob\Sparqler\Terms\Subquery;
use MadBob\Sparqler\Blocks\Triple;
use MadBob\Sparqler\Blocks\Where;
use MadBob\Sparqler\Blocks\Minus;
use MadBob\Sparqler\Blocks\WhereOptional;
use MadBob\Sparqler\Blocks\WhereRaw;
use MadBob\Sparqler\Blocks\FilterFunction;
use MadBob\Sparqler\Blocks\FilterQuery;
use MadBob\Sparqler\Utils\StatementsBag;
use MadBob\Sparqler\Utils\Reasoner;
use MadBob\Sparqler\Utils\TermsArray;
use MadBob\Sparqler\Building\BuildingRollback;
use MadBob\Sparqler\Building\BuilderStack;
use MadBob\Sparqler\Building\WikiDataBuilder;
use EasyRdf\Literal as ELiteral;
use EasyRdf\Resource as EResource;
use EasyRdf\Graph as EGraph;

class Builder
{
    use BuilderStack;
    use WikiDataBuilder;

    public const BUILDING_STEP_INIT = 0;
    public const BUILDING_STEP_MODIFIERS = 1;
    public const BUILDING_STEP_FILTERS = 2;
    public const BUILDING_STEP_PAGINATION = 3;
    public const BUILDING_STEP_FINAL = 4;

    private $internalId;
    private $client;

    private $prefix = '';

    /*
        This is the current function for the builder: select, insert, delete...
        Is defined by Client, for each different initialization method
    */
    private $mode;

    private $graph;
    protected $mode_vars;
    private $select_vars;

    private $subject = null;

    private $wheres = null;
    private $infere = false;

    protected $limit = 0;
    protected $offset = 0;
    protected $orderBy = null;
    protected $groupBy = null;

    /*
        Here we hold a relation among required predicates and dynamically
        assigned Variables, to ensure to always use the same references while
        building the final query
    */
    private $predicatesMap = [];

    private $processing_parts = [];
    private $processing_stack = [];
    private $current_process_step = 0;

    public function __construct($client)
    {
        /*
            Each Builder instance has his own internal identifier, as an easy
            way to check identity with other Builders instances (eventually
            built when nesting subqueries)
        */
        $this->internalId = Utils::randomString();

        $this->client = $client;

        $prefixes = [];
        $namespaces = \EasyRdf\RdfNamespace::namespaces();
        foreach ($namespaces as $prefix => $uri) {
            $prefixes[] = sprintf('PREFIX %s: <%s>', $prefix, $uri);
        }

        $this->prefix = join("\n", $prefixes) . "\n";
        $this->wheres = [];
    }

    public function setMasterSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function getMasterSubject()
    {
        if (is_null($this->subject)) {
            $this->subject = new Variable();
            $this->subject->setBuilder($this);
        }

        return $this->subject;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function sameAs($builder)
    {
        return $this->internalId == $builder->internalId;
    }

    private function wireModeVars()
    {
        $new = [];

        foreach ($this->mode_vars as $var) {
            if (is_a($var, Term::class)) {
                $var = $this->enclose($var);
            }

            $new[] = $var;
        }

        $this->mode_vars = $new;
    }

    public function setMode($mode, $vars = [])
    {
        $this->mode = $mode;
        $this->mode_vars = $vars;
        $this->wireModeVars();
    }

    public function setGraph($graph)
    {
        $this->graph = $graph;
    }

    public function enclose($value, $preferred = null)
    {
        $ret = null;

        if (is_a($value, Term::class)) {
            $ret = $value;
        }
        elseif (is_a($value, ELiteral::class)) {
            $ret = new Literal($value);
        }
        elseif (is_array($value)) {
            if (isset($value['type'])) {
                /*
                    Internally, EasyRdf uses indexed array to rappresent
                    informations. See Graph::add() for more details.
                    Here we guess the type of the given value and convert to a
                    Term
                    cfr. EasyRdf\Graph::checkValueParam()
                */
                switch ($value['type']) {
                    case 'bnode':
                    case 'uri':
                        $ret = new Iri($value['value']);
                        break;
                    case 'literal':
                        /*
                            TODO: when a string is assigned directly as property
                            value for a Resource, and then the Graph is
                            committed, I'm in this case. But EasyRdf's Literal
                            do not escapes quotes from string, breaking the
                            whole query. Perhaps here can be added some kind of
                            check
                        */
                        $value = ELiteral::create($value['value'], $value['lang'] ?? null, $value['datatype'] ?? null);
                        $ret = new Literal($value);
                        break;
                    default:
                        echo "Broken array type\n";
                        print_r($value);
                        die();
                }
            }
            else {
                $ret = [];

                foreach ($value as $v) {
                    $a = $this->enclose($v, $preferred);
                    $a->setBuilder($this);
                    $ret[] = $a;
                }

                return $ret;
            }
        }
        elseif (is_a($value, EResource::class)) {
            $ret = new Iri($value->getUri());
        }
        elseif (is_callable($value)) {
            $ret = new Subquery($value);
        }
        elseif (is_string($value)) {
            if (substr($value, 0, 1) == '?') {
                $ret = new Variable(substr($value, 1));
            }
            elseif (strncmp($value, 'enclose://', 10) == 0) {
                $value = substr($value, 10);
                $separator = strpos($value, ':');
                $type = substr($value, 0, $separator);
                $value = substr($value, $separator + 1);

                switch ($type) {
                    case 'float':
                        return $this->enclose((float) $value);

                    case 'integer':
                        return $this->enclose((int) $value);

                    case 'iri':
                        return $this->enclose(new Iri($value));

                    default:
                        throw new \InvalidArgumentException("Unrecognized enclose:// expression");
                }
            }
        }

        if (is_null($ret)) {
            if ($preferred) {
                $ret = new $preferred($value);
            }
            else {
                if (gettype($value) == 'string') {
                    $ret = new Plain($value);
                }
                else {
                    $literal_datatype = ELiteral::getDatatypeForValue($value);
                    if ($literal_datatype) {
                        $ret = new Literal(ELiteral::create($value));
                    }
                    else {
                        $ret = new Raw($value);
                    }
                }
            }
        }

        $ret->setBuilder($this);
        return $ret;
    }

    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function offset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    public function orderBy($predicate, $direction = 'asc')
    {
        $this->orderBy = (object) [
            'predicate' => $this->enclose($predicate, Iri::class),
            'direction' => $direction,
        ];

        return $this;
    }

    public function groupBy($groups)
    {
        $this->groupBy = new TermsArray($this, Iri::class);
        foreach ($groups as $g) {
            $this->groupBy->add($g);
        }

        return $this;
    }

    public function prefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    public function infere()
    {
        $this->infere = true;
        return $this;
    }

    public function merge($builder)
    {
        $this->wheres = array_merge($this->wheres, $builder->wheres);
    }

    public function where($predicate, $operator, $object = null)
    {
        if (is_null($object)) {
            $object = $operator;
            $operator = '=';
        }

        if (is_a($predicate, Optional::class)) {
            return $this->whereOptional($predicate, $operator, $object);
        }
        else {
            $this->wheres[] = new Where($this, $this->enclose($predicate, Iri::class), $operator, $this->enclose($object));
            return $this;
        }
    }

    public function whereReverse($predicate, $subject)
    {
        return $this->where($this->enclose($subject, Variable::class), $this->enclose($predicate, Iri::class), new OwnSubject());
    }

    public function whereIn($predicate, $options)
    {
        $enclosed = [];
        foreach ($options as $opt) {
            $enclosed[] = $this->enclose($opt, Plain::class);
        }

        return $this->where($predicate, 'in', $enclosed);
    }

    public function whereOptional($predicate, $operator, $object = null)
    {
        if (is_null($object)) {
            $object = $operator;
            $operator = '=';
        }

        $this->wheres[] = new WhereOptional($this, $this->enclose($predicate, Iri::class), $operator, $this->enclose($object));
        return $this;
    }

    public function whereRaw($condition)
    {
        $this->wheres[] = new WhereRaw($this, $condition);
        return $this;
    }

    public function minus($callable)
    {
        $this->wheres[] = new Minus($this, $callable);
        return $this;
    }

    private function addFilter($predicate, $operator, $object, $condition)
    {
        if (is_callable($predicate)) {
            $filter = new FilterQuery($this, $predicate);
        }
        else {
            if (is_null($object)) {
                $object = $operator;
                $operator = $predicate;
                $predicate = $this->getMasterSubject();
            }

            if (is_null($object)) {
                $object = new Variable();
            }

            $filter = new FilterFunction($this, $this->enclose($predicate, Iri::class), $this->enclose($operator, Raw::class), $this->enclose($object));
        }

        $filter->setCondition($condition);
        $this->wheres[] = $filter;

        return $this;
    }

    public function filter($predicate, $operator = null, $object = null)
    {
        $this->addFilter($predicate, $operator, $object, '');
        return $this;
    }

    public function filterNotExists($predicate, $operator = null, $object = null)
    {
        $this->addFilter($predicate, $operator, $object, 'NOT EXISTS');
        return $this;
    }

    public function filterLang($predicate, $language)
    {
        $this->addFilter($predicate, 'lang', $this->enclose($language, Plain::class), '');
        return $this;
    }

    public function filterRegex($predicate, $expression)
    {
        $this->addFilter($predicate, 'regex', $this->enclose($expression, Plain::class), '');
        return $this;
    }

    public function mapPredicateToVariable($predicate)
    {
        $predicate = $predicate->compile();

        foreach ($this->predicatesMap as $iri => $variable) {
            if ($iri == $predicate) {
                return [$variable, true];
            }
        }

        $variable = new Variable();
        $this->predicatesMap[$predicate] = $variable;
        return [$variable, false];
    }

    private function mapTransformParameter($statements, $parameter)
    {
        if (is_a($parameter, OwnSubject::class) || is_a($parameter, Variable::class) || is_a($parameter, Raw::class)) {
            $ret = $parameter;
        }
        else {
            list($ret, $exists) = $this->mapPredicateToVariable($parameter);
            if ($exists === false) {
                $statements->addTriple(new Triple($this, $this->getMasterSubject(), $parameter, $ret));
            }
        }

        return $ret;
    }

    private function startQueryString($start)
    {
        if (empty($this->prefix)) {
            return $start;
        }
        else {
            return sprintf('%s %s', $this->prefix, $start);
        }
    }

    protected function currentStatements()
    {
        $index = min($this->current_process_step - 1, 0);
        return $this->processing_stack[$index];
    }

    private function build()
    {
        /*
            Here the construction of the complete query in splitted in different
            steps. If one or more building elements (blocks, functions,
            terms...) have to alter some information, adding some mode var or a
            where() statement, those may raise a BuildingRollback exception with
            the step from which restart computation
        */
        try {
            switch ($this->current_process_step) {
                case Builder::BUILDING_STEP_INIT:
                    /*
                        Variables to select
                    */

                    $statements = new StatementsBag($this);
                    $this->processing_stack[0] = $statements;
                    $this->predicatesMap = [];
                    $final = [];

                    switch ($this->mode) {
                        case 'select_distinct':
                        case 'select':
                            $this->select_vars = [];
                            $master_subject = $this->getMasterSubject();

                            if ($this->mode == 'select') {
                                $final[] = $this->startQueryString('SELECT');
                            }
                            else {
                                $final[] = $this->startQueryString('SELECT DISTINCT');
                            }

                            foreach ($this->mode_vars as $index => $var) {
                                if ($var == '*') {
                                    $field_var = new Variable();
                                    $value_var = new Variable('var' . $index);
                                    $this->select_vars[] = $field_var;
                                    $this->select_vars[] = $value_var;
                                    $statements->addTriple(new Triple($this, $master_subject, $field_var, $value_var));
                                }
                                elseif (is_a($var, Optional::class)) {
                                    list($value_var, $exists) = $this->mapPredicateToVariable($var);
                                    $this->select_vars[] = $value_var;

                                    if ($exists === false) {
                                        $statements->addTriple(new WhereOptional($this, $var, '=', $value_var));
                                    }
                                }
                                elseif (is_a($var, Variable::class) || is_a($var, OwnSubject::class) || is_a($var, Raw::class)) {
                                    $this->select_vars[] = $var;
                                }
                                elseif (is_a($var, Aggregate::class)) {
                                    $replace_aggregate = $this->mapTransformParameter($statements, $var->getTarget());
                                    $var->wireBack($replace_aggregate);
                                    $this->select_vars[] = $var;
                                }
                                else {
                                    /*
                                        Note: for each intended property, here we generate a
                                        new random variable whose map variable -> predicate
                                        is appended into the wheres list.
                                        This map is reverted in get(), to reassign to each
                                        variable the properly named predicate
                                    */
                                    $predicate = $this->enclose($var, Iri::class);
                                    list($value_var, $exists) = $this->mapPredicateToVariable($predicate);
                                    $this->select_vars[] = $value_var;

                                    if ($exists === false) {
                                        $statements->addTriple(new Triple($this, $master_subject, $predicate, $value_var));
                                    }
                                }
                            }

                            $final[] = join(' ', array_map(function ($selected) {
                                return $selected->compile();
                            }, $this->select_vars));

                            if ($this->graph) {
                                $final[] = sprintf('FROM %s', (new Iri($this->graph))->compile());
                            }

                            break;

                        case 'construct':
                            $final[] = $this->startQueryString('CONSTRUCT');

                            if (empty($this->mode_vars)) {
                                $statements->add([$this->getMasterSubject(), new Variable(), new Variable()]);
                            }
                            else {
                                foreach ($this->mode_vars as $index => $var) {
                                    $statements->add($var);
                                }
                            }

                            $final[] = sprintf('{ %s }', $statements->compile());

                            if ($this->graph) {
                                $final[] = sprintf('FROM %s', (new Iri($this->graph))->compile());
                            }

                            break;

                        case 'delete':
                            $final[] = $this->startQueryString('DELETE');

                            if ($this->graph) {
                                $final[] = sprintf('FROM %s', (new Iri($this->graph))->compile());
                            }

                            $select_statements = new StatementsBag($this);

                            /*
                                If no mode vars are passed at initialization, it
                                is the short-hand form for "delete all about
                                entities matching the 'where' conditions"
                            */
                            if (empty($this->mode_vars) && empty($this->wheres) === false) {
                                $glue = new Triple($this, new OwnSubject(), new Variable(), new Variable());
                                $select_statements->add($glue);
                                $this->wheres[] = $glue;
                            }
                            else {
                                foreach ($this->mode_vars as $index => $var) {
                                    $select_statements->add($var);
                                }

                                if (empty($this->wheres)) {
                                    $statements = $select_statements->filterWithVariables();
                                }
                            }

                            $final[] = sprintf('{ %s }', $select_statements->compile());

                            break;

                        case 'insert':
                            $select_statements = new StatementsBag($this);
                            foreach ($this->mode_vars as $index => $var) {
                                $select_statements->add($var);
                            }

                            $insert_data = false;

                            /*
                                If the INSERT statement has no filters, we
                                handle it as an INSERT DATA to push the whole
                                block of triples into the store
                            */
                            if (empty($this->wheres)) {
                                $statements = $select_statements->filterWithVariables();
                                if ($statements->isEmpty()) {
                                    $insert_data = true;
                                }
                            }

                            if ($insert_data) {
                                $final[] = $this->startQueryString('INSERT DATA');
                            }
                            else {
                                $final[] = $this->startQueryString('INSERT');
                            }

                            if ($this->graph) {
                                $final[] = sprintf('INTO %s', (new Iri($this->graph))->compile());
                            }

                            $final[] = sprintf('{ %s }', $select_statements->compile());

                            break;

                        case 'subquery':
                            break;

                        default:
                            throw new \UnexpectedValueException("Unexpected Builder mode");
                    }

                    $this->processing_parts[0] = $final;
                    $this->processing_stack[0] = $statements;
                    $this->current_process_step = 1;

                case Builder::BUILDING_STEP_MODIFIERS:
                    /*
                        Query modifiers (order by, group by...).
                        Those are put before elaboration of wheres, as other statements
                        may be pushed to append missing variables into the query
                    */

                    $statements = clone $this->processing_stack[0];
                    $final = [];

                    if ($this->groupBy) {
                        $grouping_objects = [];

                        foreach ($this->groupBy as $group) {
                            $grouping_objects[] = $this->mapTransformParameter($statements, $group);
                        }

                        $final[] = sprintf('GROUP BY %s', join(' ', array_map(function ($g) {
                            return $g->compile();
                        }, $grouping_objects)));
                    }

                    if ($this->orderBy) {
                        $sorting_object = $this->mapTransformParameter($statements, $this->orderBy->predicate);
                        $final[] = sprintf('ORDER BY %s(%s)', $this->orderBy->direction, $sorting_object->compile());
                    }

                    $this->processing_parts[1] = $final;
                    $this->processing_stack[1] = $statements;
                    $this->current_process_step = 2;

                case Builder::BUILDING_STEP_FILTERS:
                    /*
                        Filters (wheres)
                    */

                    $statements = clone $this->processing_stack[1];
                    $final = [];

                    foreach ($this->wheres as $where) {
                        $statements->addTriple($where);
                    }

                    if ($this->infere) {
                        $reasoner = new Reasoner($this);
                        $reasoner->infere($statements);
                    }

                    if ($statements->isEmpty() === false) {
                        if ($this->mode == 'subquery') {
                            $final[] = $statements->compile();
                        }
                        else {
                            $final[] = sprintf('WHERE { %s }', $statements->compile());
                        }
                    }

                    $this->processing_parts[2] = $final;
                    $this->processing_stack[2] = $statements;
                    $this->current_process_step = 3;

                case Builder::BUILDING_STEP_PAGINATION:
                    /*
                        Pagination (limit, offset...)
                    */

                    $statements = clone $this->processing_stack[2];
                    $final = [];

                    if ($this->offset) {
                        $final[] = ' OFFSET ' . $this->offset;
                    }

                    if ($this->limit) {
                        $final[] = ' LIMIT ' . $this->limit;
                    }

                    $this->processing_parts[3] = $final;
                    $this->processing_stack[3] = $statements;
                    $this->current_process_step = 4;

                case Builder::BUILDING_STEP_FINAL:
                    /*
                        Running final callback required by
                        functions/blocks/terms.
                        If any, those usually produce a BuildingRollback
                    */

                    $statements = clone $this->processing_stack[3];
                    $this->runFinal();
                    $this->processing_stack[4] = $statements;
                    $this->current_process_step = 5;

                    /*
                        This breaks the whole switch. The default case is just
                        for superstition
                    */
                    break;

                default:
                    throw new \UnexpectedValueException("Unexpected step in Builder");
            }
        }
        catch (BuildingRollback $e) {
            $this->current_process_step = $e->nextStep();
            $this->client->getLogger()->debug('Resetting query building to step ' . $this->current_process_step);
            return $this->build();
        }

        return join(' ', [
            trim(join(' ', $this->processing_parts[0])),
            trim(join(' ', $this->processing_parts[2])),
            trim(join(' ', $this->processing_parts[1])),
            trim(join(' ', $this->processing_parts[3])),
        ]);
    }

    public function toString()
    {
        return $this->compile();
    }

    public function compile()
    {
        return $this->build();
    }

    public function getRaw()
    {
        $sparql = $this->build();
        return $this->client->doRaw($sparql);
    }

    private function expectsGraph()
    {
        return in_array($this->mode, ['construct']);
    }

    public function get()
    {
        $result = $this->getRaw();

        if (is_a($result, Graph::class) || $result->getType() == 'boolean') {
            return $result;
        }
        elseif ($this->expectsGraph()) {
            /*
                Wikidatism: when asking for a graph (usually: in a CONSTRUCT
                query), a result set of type application/sparql-results+json is
                returned, composed by subject/predicate/object attributes,
                instead of a proper graph.
                Here we rebuild the Graph from those data
            */
            $final = new Graph($this->client->getQueryUri());
            $final->internalOps(true);

            foreach ($result as $resource) {
                $final->add($resource->subject, $resource->predicate, $resource->object);
            }

            $final->internalOps(false);
            return $final;
        }

        $statements = $this->currentStatements();
        if ($statements) {
            $master_subject = $this->getMasterSubject();
            $master_subject_name = substr($master_subject->compile(), 1);
            $map = [];

            foreach ($result->getFields() as $field) {
                if ($field == $master_subject_name) {
                    if ($master_subject->isExplicit()) {
                        $map[$field] = $master_subject_name;
                    }
                    else {
                        $map[$field] = 'subject';
                    }
                }
                else {
                    $field_var = new Variable($field);
                    $target_predicate = $statements->matchingObject($field_var);

                    if ($target_predicate) {
                        $map[$field] = $target_predicate->compile();
                    }
                    else {
                        $target_predicate = $statements->matchingSubject($field_var);

                        if ($target_predicate) {
                            $map[$field] = $target_predicate->compile() . ' relation';
                        }
                        else {
                            $target = $this->findVariable($field_var);
                            if ($target && $target->translate()) {
                                $map[$field] = $target->translate();
                            }
                        }
                    }
                }
            }

            foreach ($result as $index => $row) {
                $traslated = (object) [];

                foreach ($row as $prop => $value) {
                    $t = $map[$prop] ?? $prop;
                    $traslated->$t = $value;
                }

                $result->offsetSet($index, $traslated);
            }
        }

        return $result;
    }

    public function run()
    {
        $sparql = $this->build();
        $this->client->update($sparql);
    }

    public function queue()
    {
        $this->client->queue($this);
    }

    public function count($target = null)
    {
        if (is_null($target)) {
            $target = new Raw('*');
        }

        /*
            Just in case a different mode has been defined when requiring a
            count()...
        */
        $this->mode = 'select';

        $this->mode_vars = [new Aggregate('count', $target)];
        $this->wireModeVars();
        $sparql = $this->build();
        $response = $this->client->query($sparql);

        foreach ($response as $res) {
            foreach ($res as $field) {
                return $field->getValue();
            }
        }

        return 0;
    }
}
