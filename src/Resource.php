<?php

namespace MadBob\Sparqler;

use MadBob\Sparqler\Terms\Term;
use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Terms\Variable;
use MadBob\Sparqler\Terms\OwnSubject;
use EasyRdf\Resource as EResource;
use EasyRdf\Literal as ELiteral;

class Resource extends EResource
{
    public function iri()
    {
        return new Iri($this->getUri());
    }

    private function wrapIri($target)
    {
        if (is_a($target, Term::class) === false) {
            $target = new Iri($target);
        }

        return $target;
    }

    private function relationQuery($subject, $property, $object)
    {
        return $this->graph->getClient()->doConstruct()->where($subject, $property, $object);
    }

    public function relation($property)
    {
        $property = $this->wrapIri($property);
        return $this->relationQuery(new Iri($this->getUri()), $property, new OwnSubject());
    }

    public function related($class, $property = null)
    {
        $class_type = $this->wrapIri($class);

        if (is_null($property)) {
            $property = new Variable();
        }
        else {
            $property = $this->wrapIri($property);
        }

        return $this->relation($property)->where('rdf:type', $class_type);
    }

    public function relationReverse($property)
    {
        $property = $this->wrapIri($property);
        return $this->relationQuery(new OwnSubject(), $property, new Iri($this->getUri()));
    }

    public function relatedReverse($class, $property = null)
    {
        $class_type = $this->wrapIri($class);

        if (is_null($property)) {
            $property = new Variable();
        }
        else {
            $property = $this->wrapIri($property);
        }

        return $this->relationReverse($property)->where('rdf:type', $class_type);
    }

    public function p($property)
    {
        $value = $this->get($property);

        if (is_a($value, ELiteral::class)) {
            return $value->getValue();
        }
        else {
            return $value;
        }
    }
}
