<?php

namespace MadBob\Sparqler;

use MadBob\Sparqler\Terms\Iri;
use MadBob\LovAPI\LovAPI;

class Ontology
{
    private $client = null;
    private $map = null;

    private $to_parse = [];
    private $to_parse_index = -1;

    private function saveCache()
    {
        $cache = $this->client->getCache();
        if ($cache) {
            $cache->set('sparqler_introspector_graph', $this->map);
        }
    }

    private function verifyConfig($config)
    {
        /*
            Here we enforce the namespaces for ontologies used in inference
        */

        if (!isset($config['namespaces']['owl'])) {
            $config['namespaces']['owl'] = 'http://www.w3.org/2002/07/owl#';
            \EasyRdf\RdfNamespace::set('owl', 'http://www.w3.org/2002/07/owl#');
        }

        if (!isset($config['namespaces']['rdfs'])) {
            $config['namespaces']['rdfs'] = 'http://www.w3.org/2000/01/rdf-schema#';
            \EasyRdf\RdfNamespace::set('rdfs', 'http://www.w3.org/2000/01/rdf-schema#');
        }

        return $config;
    }

    public function __construct($client, $config)
    {
        $this->client = $client;
        $cache = $this->client->getCache();
        $config = $this->verifyConfig($config);

        if ($cache) {
            $required_namespaces_key = array_keys($config['namespaces']);
            sort($required_namespaces_key);
            $required_namespaces_key = join(',', $required_namespaces_key);
            $loaded_ontologies = $cache->get('sparqler_introspector_graph_ontologies');

            if ($loaded_ontologies == $required_namespaces_key) {
                $this->map = $cache->get('sparqler_introspector_graph');
                return;
            }

            $cache->set('sparqler_introspector_graph_ontologies', $required_namespaces_key);
            $cache->delete('sparqler_introspector_graph');
        }

        $graph = new Graph();
        $lov = new LovAPI();

        foreach ($config['namespaces'] as $prefix => $url) {
            $file = $lov->fetchFile($url);
            if ($file) {
                $graph->parse($file);
            }
        }

        if (isset($config['extra_ontologies'])) {
            foreach ($config['extra_ontologies'] as $o) {
                $graph->parseFile($o);
            }
        }

        $this->to_parse = ['http://www.w3.org/2000/01/rdf-schema#Class'];
        $this->to_parse_index = 0;
        $classes = $this->parseClasses($graph);

        $this->to_parse = ['http://www.w3.org/1999/02/22-rdf-syntax-ns#Property'];
        $this->to_parse_index = 0;
        $properties = $this->parseProperties($graph);

        $this->map = array_merge($classes, $properties);
        $this->saveCache();
    }

    private function parseClasses($graph)
    {
        $classes = [];

        while (true) {
            $bc = $this->to_parse[$this->to_parse_index] ?? null;
            if (is_null($bc)) {
                break;
            }

            foreach ($graph->allOfType($bc) as $resource) {
                $this->generateClass($resource, $classes);
            }

            $this->to_parse_index++;
        }

        return $classes;
    }

    private function parseProperties($graph)
    {
        $classes = [];

        while (true) {
            $bc = $this->to_parse[$this->to_parse_index] ?? null;
            if (is_null($bc)) {
                break;
            }

            foreach ($graph->allOfType($bc) as $resource) {
                $this->generateProperty($resource, $classes);
            }

            $this->to_parse_index++;
        }

        return $classes;
    }

    private function wire(&$all, $from, $to, $type)
    {
        if (!isset($all[$from])) {
            $this->to_parse[] = $from;

            $all[$from] = [
                'parents' => [],
                'children' => [],
                'equivalents' => [],
                'inverses' => [],
            ];
        }

        if (in_array($to, $all[$from][$type]) === false) {
            $all[$from][$type][] = $to;
        }
    }

    private function checkConnection(&$all, $resource, $property, $from_group, $to_group)
    {
        $class_id = $resource->getUri();

        $links = $resource->all($property);
        foreach ($links as $link) {
            $link_id = $link->getUri();
            $this->wire($all, $link_id, $class_id, $from_group);
            $this->wire($all, $class_id, $link_id, $to_group);
        }
    }

    private function generateClass($resource, &$classes)
    {
        $this->checkConnection($classes, $resource, 'rdfs:subClassOf', 'children', 'parents');
        $this->checkConnection($classes, $resource, 'owl:equivalentClass', 'equivalents', 'equivalents');
    }

    private function generateProperty($resource, &$properties)
    {
        $this->checkConnection($properties, $resource, 'rdfs:subPropertyOf', 'children', 'parents');
        $this->checkConnection($properties, $resource, 'owl:equivalentProperty', 'equivalents', 'equivalents');
        $this->checkConnection($properties, $resource, 'owl:inverseOf', 'inverses', 'inverses');
    }

    public function optimize()
    {
        /*
            TODO: query the current SPARQL endpoint to ASK for mapped classes
            and properties, and remove from the map those that do not actually
            exist
        */
    }

    public function getByIri($property)
    {
        if (is_a($property, Iri::class)) {
            $property = $property->expanded();
        }

        return $this->map[$property] ?? null;
    }
}
