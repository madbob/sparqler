<?php

/*
    Original code:

    EasyRdf
    Copyright (c) Nicholas J Humfrey
    https://www.opensource.org/licenses/bsd-license.php
*/

namespace MadBob\Sparqler;

class Collection extends Resource implements \ArrayAccess, \Countable, \SeekableIterator
{
    private $position;
    private $current;

    public function __construct($uri, $graph)
    {
        $this->position = 1;
        $this->current = null;
        parent::__construct($uri, $graph);
    }

    public function seek($position)
    {
        if (is_int($position) && $position > 0) {
            list($node, $actual) = $this->getCollectionNode($position);
            if ($actual === $position) {
                $this->position = $actual;
                $this->current = $node;
            }
            else {
                throw new \OutOfBoundsException("Unable to seek to position $position in the collection");
            }
        }
        else {
            throw new \InvalidArgumentException("Collection position must be a positive integer");
        }
    }

    public function rewind()
    {
        $this->position = 1;
        $this->current = null;
    }

    public function current()
    {
        if ($this->position === 1) {
            return $this->get('rdf:first');
        }
        elseif ($this->current) {
            return $this->current->get('rdf:first');
        }
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        if ($this->position === 1) {
            $this->current = $this->get('rdf:rest');
        }
        elseif ($this->current) {
            $this->current = $this->current->get('rdf:rest');
        }

        $this->position++;
    }

    public function valid()
    {
        if ($this->position === 1 && $this->hasProperty('rdf:first')) {
            return true;
        }
        elseif ($this->current !== null && $this->current->hasProperty('rdf:first')) {
            return true;
        }
        else {
            return false;
        }
    }

    public function getCollectionNode($offset)
    {
        $position = 1;
        $node = $this;
        $nil = $this->graph->resource('rdf:nil');

        while (($rest = $node->get('rdf:rest')) && $rest !== $nil && (is_null($offset) or ($position < $offset))) {
            $node = $rest;
            $position++;
        }

        return array($node, $position);
    }

    public function count()
    {
        list($node, $position) = $this->getCollectionNode(null);
        if (!$node->hasProperty('rdf:first')) {
            return 0;
        }
        else {
            return $position;
        }
    }

    public function append($value)
    {
        list($node, ) = $this->getCollectionNode(null);
        $rest = $node->get('rdf:rest');

        if ($node === $this && is_null($rest)) {
            $node->set('rdf:first', $value);
            $node->addResource('rdf:rest', 'rdf:nil');
        }
        else {
            $new = $this->graph->newBnode();
            $node->set('rdf:rest', $new);
            $new->add('rdf:first', $value);
            $new->addResource('rdf:rest', 'rdf:nil');
        }

        return 1;
    }

    public function offsetExists($offset)
    {
        if (is_int($offset) && $offset > 0) {
            list($node, $position) = $this->getCollectionNode($offset);
            return ($node && $position === $offset && $node->hasProperty('rdf:first'));
        }
        else {
            throw new \InvalidArgumentException("Collection offset must be a positive integer");
        }
    }

    public function offsetGet($offset)
    {
        if (is_int($offset) && $offset > 0) {
            list($node, $position) = $this->getCollectionNode($offset);
            if ($node && $position === $offset) {
                return $node->get('rdf:first');
            }
        }
        else {
            throw new \InvalidArgumentException("Collection offset must be a positive integer");
        }
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->append($value);
        }
        elseif (is_int($offset) && $offset > 0) {
            list($node, $position) = $this->getCollectionNode($offset);

            while ($position < $offset) {
                $new = $this->graph->newBnode();
                $node->set('rdf:rest', $new);
                $new->addResource('rdf:rest', 'rdf:nil');
                $node = $new;
                $position++;
            }

            if (!$node->hasProperty('rdf:rest')) {
                $node->addResource('rdf:rest', 'rdf:nil');
            }

            return $node->set('rdf:first', $value);
        }
        else {
            throw new \InvalidArgumentException("Collection offset must be a positive integer");
        }
    }

    public function offsetUnset($offset)
    {
        if (is_int($offset) && $offset > 0) {
            list($node, $position) = $this->getCollectionNode($offset);
        }
        else {
            throw new \InvalidArgumentException("Collection offset must be a positive integer");
        }

        if ($node && $position === $offset) {
            $nil = $this->graph->resource('rdf:nil');
            if ($position === 1) {
                $rest = $node->get('rdf:rest');

                if ($rest && $rest !== $nil) {
                    $node->set('rdf:first', $rest->get('rdf:first'));
                    $node->set('rdf:rest', $rest->get('rdf:rest'));
                    $rest->delete('rdf:first');
                    $rest->delete('rdf:rest');
                }
                else {
                    $node->delete('rdf:first');
                    $node->delete('rdf:rest');
                }
            }
            else {
                $node->delete('rdf:first');
                $rest = $node->get('rdf:rest');
                $previous = $node->get('^rdf:rest');

                if (is_null($rest)) {
                    $rest = $nil;
                }

                if ($previous) {
                    $previous->set('rdf:rest', $rest);
                }
            }
        }
    }
}
