<?php

/*
    Here are collected a few utilities to handle the flow of query construction
*/

namespace MadBob\Sparqler\Building;

trait BuilderStack
{
    private $finals = [];

    /*
        This is intended to be used only after the query has been executed, and
        the $mode_vars of the Builder have been elaborated and replaced with
        encapsulated Terms
    */
    protected function findVariable($var)
    {
        foreach ($this->select_vars as $mv) {
            if ($mv->sameAs($var)) {
                return $mv;
            }
        }

        return $this->currentStatements()->findVariable($var);
    }

    public function queueFinal($callable)
    {
        $this->finals[] = $callable;
    }

    protected function runFinal()
    {
        $next_step = 10;

        foreach ($this->finals as $as) {
            $ret = $as($this);

            if ($ret != -1) {
                if ($ret < $next_step) {
                    $next_step = $ret;
                }
            }
        }

        $this->finals = [];

        if ($next_step > -1 && $next_step < 10) {
            throw new BuildingRollback($next_step);
        }
    }
}
