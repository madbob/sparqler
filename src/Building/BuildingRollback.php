<?php

namespace MadBob\Sparqler\Building;

class BuildingRollback extends \Exception
{
    private $next = 0;

    public function __construct($next_step)
    {
        $this->next = $next_step;
    }

    public function nextStep()
    {
        return $this->next;
    }
}
