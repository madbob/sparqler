<?php

/*
    Here are collected a few utilities specific for WikiData to be used in the
    Builder
*/

namespace MadBob\Sparqler\Building;

use MadBob\Sparqler\Builder;
use MadBob\Sparqler\Terms\Term;
use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Terms\OwnSubject;
use MadBob\Sparqler\Terms\Variable;

trait WikiDataBuilder
{
    /*
        TODO: handle descriptions (like ?varLabel, as ?varDescription)
    */

    public function withWikiDataLabels($langs = ['en'], $targets = ['subject'])
    {
        /*
            We have to defer the map of label variables after the $mode_vars of
            the SELECT Builder have been exploded into the StatementsBag
        */
        $this->queueFinal(function ($builder) use ($langs, $targets) {
            $actual_appended = false;

            foreach ($targets as $target) {
                if ($target == 'subject' || is_a($target, OwnSubject::class)) {
                    $ref_variable = new OwnSubject();
                    $ref_variable->setBuilder($this);

                    if (is_a($target, OwnSubject::class)) {
                        $target = 'subject';
                    }
                }
                elseif (is_a($target, Variable::class)) {
                    $ref_variable = $target;
                }
                else {
                    list($ref_variable, $exists) = $this->mapPredicateToVariable($this->enclose($target, Iri::class));
                    if ($exists === false) {
                        continue;
                    }
                }

                $var = new Variable($ref_variable->raw() . 'Label');

                if (is_a($target, Term::class)) {
                    $target = $target->raw();
                }
                $var->as($target . ' label');

                $builder->mode_vars[] = $var;
                $actual_appended = true;

                /*
                    Here we eventually append transparently the newly generated
                    Variable to the GROUP BY list of the Builder, otherwise the
                    whole query would results invalid
                */
                if ($builder->groupBy) {
                    if ($builder->groupBy->has($ref_variable)) {
                        $builder->groupBy->add($var);
                    }
                }
            }

            if ($actual_appended) {
                return Builder::BUILDING_STEP_INIT;
            }
            else {
                return -1;
            }
        });

        $langs = join(', ', $langs);
        $this->whereRaw('SERVICE wikibase:label {bd:serviceParam wikibase:language "' . $langs . '".}');
        return $this;
    }
}
