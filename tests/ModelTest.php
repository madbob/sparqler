<?php

namespace MadBob\Sparqler\Tests;

require_once('Article.php');

use PHPUnit\Framework\TestCase;
use MadBob\Sparqler\Client;

final class ModelTest extends TestCase
{
    protected function setUp(): void
    {
        $this->client = new Client([
            'host' => 'https://query.wikidata.org/sparql',
        ]);

        $this->client->setAsGlobal();
    }

    public function testCreation()
    {
        $article = new Article();
        $id = $article->id;
        $this->assertTrue(empty($id) == false);
    }
}
