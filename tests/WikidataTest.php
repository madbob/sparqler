<?php

namespace MadBob\Sparqler\Tests;

require_once('Article.php');
require_once('BasicLogger.php');

use PHPUnit\Framework\TestCase;
use MadBob\Sparqler\Client;
use MadBob\Sparqler\Resource;
use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Terms\Variable;
use MadBob\Sparqler\Terms\OwnSubject;

final class WikidataTest extends TestCase
{
    private $client;

    protected function setUp(): void
    {
        $this->client = new Client([
            'host' => 'https://query.wikidata.org/sparql',
            'namespaces' => [
                'wd' => 'http://www.wikidata.org/entity/',
                'wdt' => 'http://www.wikidata.org/prop/direct/',
                'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
                'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
                'schema' => 'http://schema.org/',
            ],
        ]);

        $this->client->setLogger(new BasicLogger());
        $this->client->setAsGlobal();
    }

    public function testConstruct()
    {
        $graph = $this->client->doConstruct()->where('wdt:P31', new Iri('wd:Q15089'))->get();
        $this->assertTrue(count($graph->resources()) > 50);
    }

    public function testFind()
    {
        $resource = $this->client->find('wd:Q220');
        $this->assertNotNull($resource);
        $this->assertFalse(empty($resource->properties()));
    }

    public function testSelectOnLabel()
    {
        $results = $this->client->doSelect(['wdt:P1082'])->where('rdfs:label', new \EasyRdf\Literal('Roma', 'it'))->limit(1)->get();
        $this->assertEquals(1, $results->numRows());

        foreach ($results as $res) {
            $property = $res->{'wdt:P1082'};
            $this->assertNotNull($property);
            $this->assertEquals('http://www.w3.org/2001/XMLSchema#decimal', $property->getDatatypeUri());
        }
    }

    public function testSubqueries()
    {
        $results = $this->client->doSelect([new OwnSubject('item'), new Variable('itemLabel')])
            ->where('wdt:P1376', function ($query) {
                $query->where('wdt:P31', new Iri('wd:Q6256'))->where('wdt:P30', new Iri('wd:Q46'));
            })
            ->whereRaw('SERVICE wikibase:label {bd:serviceParam wikibase:language "it".}')->get();

        $this->assertTrue($results->numRows() > 10);
        $this->assertTrue($results->numRows() < 100);
        $found = false;

        foreach ($results as $res) {
            if ($res->itemLabel == 'Praga') {
                $found = true;
                break;
            }
        }

        $this->assertTrue($found);
    }

    public function testModelQuery()
    {
        $articles = Article::query([['schema:about']])->where('schema:inLanguage', 'it')->limit(10)->get();
        $this->assertEquals(10, count($articles->masterResources()));

        foreach ($articles->masterResources() as $article) {
            $this->assertEquals(Article::class, get_class($article));
        }
    }

    public function testModelFind()
    {
        $article = Article::find('https://it.wikipedia.org/wiki/SPARQL');
        $this->assertEquals(Article::class, get_class($article));
        $this->assertEquals('https://it.wikipedia.org/wiki/SPARQL', $article->id);

        $wikidata = $article->get('schema:about');
        $this->assertEquals(Resource::class, get_class($wikidata));
        $this->assertEquals('http://www.wikidata.org/entity/Q54871', $wikidata->getUri());
    }
}
