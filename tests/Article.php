<?php

namespace MadBob\Sparqler\Tests;

use MadBob\Sparqler\Model;

class Article extends Model
{
    protected $class = 'schema:Article';
}
