<?php

namespace MadBob\Sparqler\Tests;

class BasicLogger extends \Psr\Log\AbstractLogger
{
    public function log($level, \Stringable|string $message, array $context = []): void
    {
        file_put_contents('log.txt', $message . "\n\n", FILE_APPEND);
    }
}
