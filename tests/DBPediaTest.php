<?php

namespace MadBob\Sparqler\Tests;

require_once('BasicLogger.php');

use PHPUnit\Framework\TestCase;
use MadBob\Sparqler\Client;
use MadBob\Sparqler\Terms\Iri;
use MadBob\Sparqler\Terms\Raw;
use MadBob\Sparqler\Terms\Variable;
use MadBob\Sparqler\Terms\OwnSubject;

final class DBPediaTest extends TestCase
{
    private $client;

    protected function setUp(): void
    {
        $this->client = new Client([
            'host' => 'https://dbpedia.org/sparql',
            'namespaces' => [
                'dbp' => 'http://dbpedia.org/property/',
                'dbr' => 'http://dbpedia.org/resource/',
                'dbo' => 'http://dbpedia.org/ontology/',
                'dct' => 'http://purl.org/dc/terms/',
                'dbc' => 'http://dbpedia.org/resource/Category:',
                'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
            ],
        ]);

        $this->client->setLogger(new BasicLogger());

        $httpclient = \EasyRdf\Http::getDefaultHttpClient();
        $httpclient->setConfig(['timeout' => 60]);
    }

    public function testTest()
    {
        $graph = $this->client->doConstruct()->where('dbo:type', new Iri('dbr:Capital_city'))->where('dbo:timeZone', new Iri('dbr:Central_European_Time'))->get();
        $this->assertTrue(count($graph->resources()) > 0);
        $this->assertTrue(count($graph->masterResources()) > 0);
    }

    public function testFind()
    {
        $resource = $this->client->find('dbr:New_York_City');
        $this->assertNotNull($resource);
        $this->assertFalse(empty($resource->properties()));
    }

    public function testRelated()
    {
        $resource = $this->client->find('dbr:New_York_City');
        $related = $resource->related(new Iri('dbo:CityDistrict'))->get();
        $this->assertNotNull($related);
        $this->assertFalse(empty($related->resources()));
    }

    public function testSubqueries()
    {
        $results = $this->client->doSelect(['dbp:name', 'dbo:populationTotal'])
            ->where('rdf:type', new Iri('dbo:City'))
            ->where('dbo:country', function ($query) {
                $query->where('dbo:timeZone', new Iri('dbr:Central_European_Time'));
            })
            ->orderBy('dbp:name', 'desc')
            ->limit(10)
            ->get();

        $this->assertEquals(10, $results->numRows());
    }

    public function testSelectIn()
    {
        $results = $this->client->doSelect(['dbp:name', 'dbo:populationTotal'])
            ->where('rdf:type', new Iri('dbo:City'))
            ->where('dbo:country', new Iri('dbr:Italy'))
            ->where('dbo:populationTotal', '>', new Raw(10000))
            ->whereIn('dct:subject', [new Iri('dbc:Cities_and_towns_in_Piedmont'), new Iri('dbc:Cities_and_towns_in_Veneto')])
            ->orderBy('dbp:name')
            ->limit(10)
            ->get();

        $this->assertEquals(10, $results->numRows());
    }

    /*
    public function testConstruct()
    {
        $graph = $this->client->doConstruct([['dbp:areaTotalSqMi'], ['dbp:website']])->where('dct:subject', new Iri('dbc:Capitals_in_Europe'))->get();
        $this->assertFalse(empty($graph->resources()));
        foreach($graph->masterResources() as $res) {
            $this->assertEquals(2, count($res->properties()));
        }
    }
    */
}
