# SPARQLER

A SPARQL ORM built on EasyRDF.

Sparqler is a PHP "Object Relational Mapping" to query SPARQL endpoints with an object-oriented interface. It is built on top of [EasyRDF](https://www.easyrdf.org/), the most popular RDF PHP library around, and uses (mostly) the same classes.

For full documentation and examples, look at https://sparqler.madbob.org/

# Install

`composer require madbob/sparqler`
